%YAML 1.2
# The Classic ARL reference experiment

---
uid: Combined Voltage Band Defender Experiment
seed: 20221026  # Seed used to initialize the random number generator
version: 3.4.1  # Target palaestrAI version
output: palaestrai-runfiles  # Directory where the run files will be saved
repetitions: 1  # How often each run will be repeated. Each repetition is a separate file
max_runs: 10  # Maximum number of runs (excluding repetitions) created by the DoE generator
definitions:  # In this section all the components will be defined
  ###########################################
  # The environment' section
  environments:
    midasmv_tar_ms:  
      environment:
        name: palaestrai_mosaik:MosaikEnvironment
        uid: midas_powergrid
        params: 
          module: midas.tools.palaestrai:Descriptor  # Always the same if you use MIDAS
          description_func: describe  # Always the same if you use MIDAS
          instance_func: get_world  # Always the same if you use MIDAS
          arl_sync_freq: &step_size 900 # &step_size 1 means to set variable step_size=1
          end: &end 7776001 # One step extra because one step gets "lost"
          silence_missing_input_connections_warning: True

          params:  # Parameters that are passed to description_func and
                    # instance_func, Part that is being sent to MIDAS directly
            name: carl_cigre_ts
            config: midas-scenarios/classic-arl.yml # relative to current working directory; the file that is called .config/midas/ClasicARL.yml on my laptop
            end: *end
            step_size: *step_size # &step_size put here from above with value 1
            start_date: 2020-05-01 00:00:00+0100 # Use ISO datestring or magic keywork 'random'
            mosaik_params: {addr: [127.0.0.1, 56781]} # You can change the port (5678) to something else (e.g. 5679) when 'address already in use'
            store_params: #everything in here overwrites what is written in classic-arl.yml in /midas-scenarios/
              buffer_size: 1000 # Every x steps, intermediate results are dumped to the midas store
              keep_old_files: true
              filename: retro_psi_midas.hdf5
      reward:
        name: midas.tools.palaestrai.rewards:RetroPsiReward 
  ###########################################
  # The agents' section
  agents:
    gandalf_ppo:
      name: Gandalf The White
      brain: 
        name: harl.ppo.brain:PPOBrain
        params: 
          max_timesteps_per_episode: 64
      muscle: 
        name: harl.ppo.muscle:PPOMuscle
        params: {}
      objective:
        name: psi_objectives.voltage_defender_objective:VoltageDefenderObjective
        params:
          beta: 2
    gandalf_ppo2:
      name: Gandalf The White
      brain:
        name: harl.ppo.brain:PPOBrain
        params:
          max_timesteps_per_episode: 64
      muscle:
        name: harl.ppo.muscle:PPOMuscle
        params: { }
      objective:
        name: psi_objectives.simple_voltage_defender_objective:SimpleVoltageBandPendulumDefender
        params: { }
    gandalf_ddpg:
      name: Gandalf The Black
      brain:
        name: harl.ddpg.brain:DDPGBrain
        params:
          gamma: 0.99
          tau: 0.005
          batch_size: 128
          alpha: 0.001 # actor
          beta: 0.001 # critic
          fc_dims: [64, 64]
          replay_size: 256
      muscle: 
        name: harl.ddpg.muscle:DDPGMuscle
        params: {}
      objective:
        name: midas.tools.palaestrai.objectives:AndreasAnnoyinglyAmicableObjective
        params: {}
    sauron_ppo:
      name: Sauron
      brain: 
        name: harl.ppo.brain:PPOBrain
        params: 
          max_timesteps_per_episode: 64
      muscle: 
        name: harl.ppo.muscle:PPOMuscle
        params: {}
      objective:
        name: psi_objectives.voltage_attacker_objective:VoltageBandViolationPendulum
        params: {}
    sauron_ddpg:
      name: Saruman
      brain:
        name: harl.ddpg.brain:DDPGBrain
        params:
          gamma: 0.99
          tau: 0.005
          batch_size: 64
          alpha: 0.001 # 0.0002
          beta: 0.001 # 0.001
          fc_dims: [64, 64] # [200, 150, 100]
          replay_size: 256
      muscle: 
        name: harl.ddpg.muscle:DDPGMuscle
        params: {}
      objective:
        name: midas.tools.palaestrai.objectives:ErikasExcitinglyEvilObjective
        params: {}
    q_controller:
      name: Gollum
      brain:
        name: palaestrai.agent.dummy_brain:DummyBrain
        params: { }
      muscle:
        name: psi_objectives.reactive_power_muscle:ReactivePowerMuscle
        params: { }
      objective:
        name: psi_objectives.voltage_defender_objective:VoltageDefenderObjective
        params:
          beta: 2
  ###########################################
  # The sensors' section
  sensors:
    all_sensors: 
      midas_powergrid:
        - midas_powergrid.Powergrid-0.0-bus-1.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-1.va_degree
        - midas_powergrid.Powergrid-0.0-bus-10.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-10.va_degree
        - midas_powergrid.Powergrid-0.0-bus-11.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-11.va_degree
        - midas_powergrid.Powergrid-0.0-bus-12.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-12.va_degree
        - midas_powergrid.Powergrid-0.0-bus-13.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-13.va_degree
        - midas_powergrid.Powergrid-0.0-bus-14.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-14.va_degree
        - midas_powergrid.Powergrid-0.0-bus-2.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-2.va_degree
        - midas_powergrid.Powergrid-0.0-bus-3.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-3.va_degree
        - midas_powergrid.Powergrid-0.0-bus-4.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-4.va_degree
        - midas_powergrid.Powergrid-0.0-bus-5.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-5.va_degree
        - midas_powergrid.Powergrid-0.0-bus-6.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-6.va_degree
        - midas_powergrid.Powergrid-0.0-bus-7.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-7.va_degree
        - midas_powergrid.Powergrid-0.0-bus-8.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-8.va_degree
        - midas_powergrid.Powergrid-0.0-bus-9.vm_pu
        - midas_powergrid.Powergrid-0.0-bus-9.va_degree
        - midas_powergrid.Powergrid-0.0-line-0.loading_percent
        - midas_powergrid.Powergrid-0.0-line-0.in_service
        - midas_powergrid.Powergrid-0.0-line-1.loading_percent
        - midas_powergrid.Powergrid-0.0-line-1.in_service
        - midas_powergrid.Powergrid-0.0-line-10.loading_percent
        - midas_powergrid.Powergrid-0.0-line-10.in_service
        - midas_powergrid.Powergrid-0.0-line-11.loading_percent
        - midas_powergrid.Powergrid-0.0-line-11.in_service
        - midas_powergrid.Powergrid-0.0-line-12.loading_percent
        - midas_powergrid.Powergrid-0.0-line-12.in_service
        - midas_powergrid.Powergrid-0.0-line-13.loading_percent
        - midas_powergrid.Powergrid-0.0-line-13.in_service
        - midas_powergrid.Powergrid-0.0-line-14.loading_percent
        - midas_powergrid.Powergrid-0.0-line-14.in_service
        - midas_powergrid.Powergrid-0.0-line-2.loading_percent
        - midas_powergrid.Powergrid-0.0-line-2.in_service
        - midas_powergrid.Powergrid-0.0-line-3.loading_percent
        - midas_powergrid.Powergrid-0.0-line-3.in_service
        - midas_powergrid.Powergrid-0.0-line-4.loading_percent
        - midas_powergrid.Powergrid-0.0-line-4.in_service
        - midas_powergrid.Powergrid-0.0-line-5.loading_percent
        - midas_powergrid.Powergrid-0.0-line-5.in_service
        - midas_powergrid.Powergrid-0.0-line-6.loading_percent
        - midas_powergrid.Powergrid-0.0-line-6.in_service
        - midas_powergrid.Powergrid-0.0-line-7.loading_percent
        - midas_powergrid.Powergrid-0.0-line-7.in_service
        - midas_powergrid.Powergrid-0.0-line-8.loading_percent
        - midas_powergrid.Powergrid-0.0-line-8.in_service
        - midas_powergrid.Powergrid-0.0-line-9.loading_percent
        - midas_powergrid.Powergrid-0.0-line-9.in_service
        - WeatherData-0.WeatherCurrent-0.t_air_deg_celsius
        - WeatherData-0.WeatherCurrent-0.bh_w_per_m2
        - WeatherData-0.WeatherCurrent-0.dh_w_per_m2
  ###########################################
  # The actuators' section
  actuators:
    attacker_actuators:
      midas_powergrid:
        - Pysimmods-0.Photovoltaic-0.p_set_mw
        - Pysimmods-0.Photovoltaic-0.q_set_mvar
        - Pysimmods-0.Photovoltaic-2.p_set_mw
        - Pysimmods-0.Photovoltaic-2.q_set_mvar
        - Pysimmods-0.Photovoltaic-3.p_set_mw
        - Pysimmods-0.Photovoltaic-3.q_set_mvar
        - Pysimmods-0.Photovoltaic-6.p_set_mw
        - Pysimmods-0.Photovoltaic-6.q_set_mvar
        - Pysimmods-0.HVAC-0.p_set_mw
        - Pysimmods-0.HVAC-1.p_set_mw
        - Pysimmods-0.CHP-0.p_set_mw
        - Pysimmods-0.CHP-1.p_set_mw
        - Pysimmods-0.CHP-2.p_set_mw
    defender_actuators:
      midas_powergrid:
        - Pysimmods-0.Photovoltaic-1.p_set_mw
        - Pysimmods-0.Photovoltaic-1.q_set_mvar
        - Pysimmods-0.Photovoltaic-4.p_set_mw
        - Pysimmods-0.Photovoltaic-4.q_set_mvar
        - Pysimmods-0.Photovoltaic-5.p_set_mw
        - Pysimmods-0.Photovoltaic-5.q_set_mvar
        - Pysimmods-0.Battery-0.p_set_mw
        - Powergrid-0.0-trafo-0.tap_pos
        - Powergrid-0.0-trafo-1.tap_pos 

  ###########################################
  # The simulation section
  simulation:
    vanilla_sim: # User-defined name of one simulation controller.
      name: palaestrai.simulation:VanillaSimController
      conditions:
        - name: palaestrai.simulation:VanillaSimControllerTerminationCondition
          params: {}
  ###########################################
  # The phase-configs' section
  phase_config:
    training:
      mode: train
      worker: 1
      episodes: 1
    test:
      mode: test
      worker: 1
      episodes: 1
  ###########################################
  # The run-configs' section
  run_config:
    condition:
      name: palaestrai.experiment:VanillaRunGovernorTerminationCondition
      params: {}

schedule:
  - phase0:
      environments: [[midasmv_tar_ms]]
      agents: [[q_controller, sauron_ppo]]
      simulation: [vanilla_sim]
      phase_config: [training]
      sensors:
        q_controller: [all_sensors]
        sauron_ppo: [all_sensors]
      actuators:
        q_controller: [defender_actuators]
        sauron_ppo: [attacker_actuators]
  - phase1:
      environments: [[midasmv_tar_ms]]  # one factor, one level
      agents: [[gandalf_ppo, sauron_ppo]]
      simulation: [vanilla_sim] # one factor, one level
      phase_config: [training] # one factor, one level
      sensors:
        gandalf_ppo: [all_sensors]
        sauron_ppo: [all_sensors]
      actuators:
        sauron_ppo: [attacker_actuators]
        gandalf_ppo: [defender_actuators]
  - phase2:
      environments: [ [ midasmv_tar_ms ] ]  # one factor, one level
      agents: [ [ gandalf_ppo2, sauron_ppo ] ]
      simulation: [ vanilla_sim ] # one factor, one level
      phase_config: [ training ] # one factor, one level
      sensors:
        gandalf_ppo2: [ all_sensors ]
        sauron_ppo: [ all_sensors ]
      actuators:
        sauron_ppo: [ attacker_actuators ]
        gandalf_ppo2: [ defender_actuators ]
  # - phaseII_defender:
  #     agents: [[gandalf_ddpg]] 
  #     sensors:
  #       gandalf_ddpg: [all_sensors]
  #       gandalf_ppo: [all_sensors]
  #     actuators:
  #       gandalf_ddpg: [defender_actuators]
  #       gandalf_ppo: [defender_actuators]
  # - phaseIII_att_def:
  #     agents: [[sauron_ppo, gandalf_ddpg]]
  #     sensors:
  #       sauron_ddpg: [all_sensors]
  #       sauron_ppo: [all_sensors]
  #       gandalf_ddpg: [all_sensors]
  #       gandalf_ppo: [all_sensors]
  #     actuators:
  #       sauron_ddpg: [attacker_actuators]
  #       sauron_ppo: [attacker_actuators]
  #       gandalf_ddpg: [defender_actuators]
  #       gandalf_ppo: [defender_actuators]
  # - phaseIV_test_att_def:
  #     phase_config: [test]