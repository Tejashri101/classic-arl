# %%
import palaestrai
import palaestrai.core
import palaestrai.store
import palaestrai.store.database_util
import palaestrai.store.database_model as paldb

import sqlalchemy as sa

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %matplotlib inline

# %%
import jsonpickle
import jsonpickle.ext.numpy as jsonpickle_numpy
jsonpickle_numpy.register_handlers()

# %%
import io
import os
import pprint
import tempfile
from pathlib import Path

# %%
# Experiment Run Meta Data
from datetime import datetime


EID = "20220818-carl1-ddpg-ppo-v1"
META_DATA = (
    "# Classic ARL\n\n"
    "* Single Phase\n"
    "* PPO Attacker\n"
    "* DDPG defender\n"
    "* 250 simulation steps\n"
    "* training only\n"
)
PLOT_PATH = os.path.abspath(os.path.join(os.getcwd(), "results", EID))
os.makedirs(PLOT_PATH, exist_ok=True)
REWARD_VOLTAGE = os.path.join(PLOT_PATH, "reward_voltage.png")
REWARD_LOADING = os.path.join(PLOT_PATH, "reward_loading.png")

with open(os.path.join(PLOT_PATH, "metadata.md"), "w") as f:
    f.write(META_DATA)
    f.write("\n\n*Created at ")
    f.write(datetime.now().strftime("%H:%M:%S%z, %Y-%m-%d"))
    f.write("*\n")

EXP_RUN_NAME = "Classic ARL single phase with PPO and DDPG"
ERI_IDX = -1

# %%
rtc = palaestrai.core.RuntimeConfig()

# %%
store_uri = f"sqlite:///{os.path.abspath(os.path.join(os.getcwd(), '..', 'palaestrai.db'))}"
rtc.__dict__["_store_uri"] = store_uri
store_uri

# %%
rtc.store_uri

# %%


# %%
dbh = palaestrai.store.Session()

# %%
q = sa.select(paldb.ExperimentRun).where(paldb.ExperimentRun.uid == EXP_RUN_NAME)
str(q)


# %%
result = dbh.execute(q).one()
experiment_run_record = result[paldb.ExperimentRun]
experiment_run_record.id, experiment_run_record.uid

# %%
experiment_run_record.experiment_run_instances

# %%
experiment_run_record.experiment_run_instances[ERI_IDX].experiment_run_phases

# %%
pd.read_sql(
    sa.select(paldb.Agent)
    .where(
        paldb.Agent.experiment_run_phase_id.in_(
            phase.id for phase in experiment_run_record.experiment_run_instances[ERI_IDX].experiment_run_phases
        )
    ),
    dbh.bind
)

# %%
run_phase_id = min(phase.id for phase in experiment_run_record.experiment_run_instances[ERI_IDX].experiment_run_phases)
run_phase_id

# %%
agent_record = dbh.execute(
    sa.select(paldb.Agent)
    .where(paldb.Agent.experiment_run_phase_id==run_phase_id)
    .where(paldb.Agent.name=="mighty_defender")
).one()[paldb.Agent]
agent_record.id, agent_record.name

# %%
actions = pd.read_sql(
    sa.select(paldb.MuscleAction).where(paldb.MuscleAction.agent_id==agent_record.id),
    dbh.bind
)
actions

# %%
len(actions.rewards[0])

# %%
def unpack_reward(x, i):
    try:
        val = x[i]["reward_value"]["value"]
    except KeyError as err:
        try:
            val = x[i]["reward_value"]["values"][0]
        except KeyError as err:
            print(f"Error at index {i}: {err}")
            print(x[i])
            raise err
    return float(val) if x else 0.0

cols = ["vm_pu_min", "vm_pu_max", "vm_pu_median", "vm_pu_mean", "vm_pu_std", "line_min", "line_max", "line_median", "line_mean", "line_std"]

for idx, name in enumerate(cols):
    actions[name] = actions.rewards.apply(lambda x: unpack_reward(x, idx))

actions




# %%
actions["vm_pu_min_mean"] = actions["vm_pu_min"].rolling(20).mean()
actions["vm_pu_max_mean"] = actions["vm_pu_max"].rolling(20).mean()
actions["vm_pu_mean_mean"] = actions["vm_pu_mean"].rolling(20).mean()
actions["vm_pu_median_mean"] = actions["vm_pu_median"].rolling(20).mean()
index = np.arange(len(actions.id))


# %%
plt.figure(figsize=(30, 6))
plt.plot(index, actions.vm_pu_min_mean, color="green", label="MIN")
plt.plot(index, actions.vm_pu_max_mean, color="red", label="MAX")
plt.plot(index, actions.vm_pu_median_mean, color="purple", label="MEDIAN")
plt.plot(index, actions.vm_pu_mean_mean, color="blue", label="MEAN")
plt.xlabel("Step Index")
plt.ylabel("Voltage Magnitude (p.u.)")
plt.title("Reward Voltage")
plt.legend()

plt.savefig(REWARD_VOLTAGE, bbox_inches="tight", dpi=300)


# %%
actions["line_min_mean"] = actions["line_min"].rolling(20).mean()
actions["line_max_mean"] = actions["line_max"].rolling(20).mean()
actions["line_mean_mean"] = actions["line_mean"].rolling(20).mean()
actions["line_median_mean"] = actions["line_median"].rolling(20).mean()

# %%
plt.figure(figsize=(30, 6))
plt.plot(index, actions.line_min_mean, color="green", label="MIN")
plt.plot(index, actions.line_max_mean, color="red", label="MAX")
plt.plot(index, actions.line_median_mean, color="purple", label="MEDIAN")
plt.plot(index, actions.line_mean_mean, color="blue", label="MEAN")
plt.xlabel("Step Index")
plt.ylabel("Loading [%]")
plt.title("Reward Line Loading")
plt.legend()

plt.savefig(REWARD_LOADING, bbox_inches="tight", dpi=300)

# %%
actions.line_max_mean

# %%



