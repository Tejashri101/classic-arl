import os

from arsenai.api.fnc_generate import generate
from palaestrai.cli.manager import cli

if __name__ == "__main__":
    runs = generate(
        "palaestrai-experiment-files/carl-reference-experiment.yml"
    )
    runfile_path = os.path.join(
        os.getcwd(), "palaestrai-runfiles", f"{runs[0]['uid'][:-2]}_run-0.yml"
    )

    cli(["start", runfile_path])
